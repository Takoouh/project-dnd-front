"use server";

import { redirect } from "next/navigation";

export async function logOutUser() {
  localStorage.removeItem("accessToken");
  redirect("/");
}
