import { NextResponse } from "next/server";

export async function POST(request: Request) {
  const { nickname, password } = await request.json();
  // Call your NestJS API to authenticate the user
  const response = await fetch(
    `${process.env.DND_API_URL}/authentication/sign-up`,
    {
      method: "POST",
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ nickname, password }),
    }
  );

  if (response.ok) {
    const data = await response.json();

    const token = data.accessToken;
    const nextResponse = NextResponse.json({ message: "Signed in", token });

    nextResponse.cookies.set("accessToken", token, {
      httpOnly: true,
      secure: true,
      maxAge: 60 * 60 * 24 * 7, // One week
      sameSite: "strict",
      path: "/",
    });

    return nextResponse;
  } else {
    return NextResponse.json(
      { message: "Authentication failed" },
      { status: 401 }
    );
  }
}
