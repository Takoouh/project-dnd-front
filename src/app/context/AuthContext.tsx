"use client";

import { apiFetch } from "@/utils/api";
import { useRouter } from "next/navigation";
import {
  ReactNode,
  createContext,
  useContext,
  useEffect,
  useState,
} from "react";
import { User } from "../lib/types/user.type";
import { Loader } from "@/components/Loader";

interface AuthContextType {
  token: string | null;
  isAuthLoading: Boolean;
  setToken: (token: string | null) => void;
  user: User | null;
  setUser: (user: User) => void;
}

const AuthContext = createContext<AuthContextType | undefined>(undefined);

export const AuthProvider = ({ children }: { children: ReactNode }) => {
  const [token, setToken] = useState<string | null>(null);
  const [isAuthLoading, setIsAuthLoading] = useState<Boolean>(true);
  const [user, setUser] = useState<User | null>(null);
  const router = useRouter();

  useEffect(() => {
    const fetchAndSetTokenAndUser = async () => {
      try {
        const tokenResponse = await fetch("/api/auth/token");

        const { token } = await tokenResponse.json();
        setToken(token);
        const user = await apiFetch("users/me", token);
        setUser(user);
      } catch {
        router.push("/login");
      }
    };
    fetchAndSetTokenAndUser().then(() => setIsAuthLoading(false));
  }, []);

  return (
    <AuthContext.Provider
      value={{ token, isAuthLoading, setToken, user, setUser }}
    >
      {isAuthLoading ? <Loader /> : children}
    </AuthContext.Provider>
  );
};

export const useAuth = (): AuthContextType => {
  const context = useContext(AuthContext);
  if (context === undefined) {
    throw new Error("useAuth must be used within an AuthProvider");
  }
  return context;
};
