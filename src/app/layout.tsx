import { NextUIProvider } from "@nextui-org/react";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { AuthProvider } from "./context/AuthContext";
import "./globals.css";
import Menu from "./ui/menu/menu";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "project-dnd",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={`min-h-screen bg-slate-800`}>
        <NextUIProvider>
          <main className={inter.className}>
            <AuthProvider>
              <Menu />
              {children}
            </AuthProvider>
          </main>
        </NextUIProvider>
      </body>
    </html>
  );
}
