import { Reward } from "./reward.type";

export type Farmable = {
  id: number;
  name: string;
  image: string;
  requiredLevel: number;
  respawnTime: number;
  reward: Reward;
};
