type UserSkillDetail = {
  level: number;
  xp: number;
  nextLevelXp: number;
};

export type UserSkills = {
  cooking: UserSkillDetail;
  fishing: UserSkillDetail;
  mining: UserSkillDetail;
  smithing: UserSkillDetail;
  woodcutting: UserSkillDetail;
};
