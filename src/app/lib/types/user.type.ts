import { UserSkills } from "./user-skills.type";

export type User = {
  nickname: string;
  skills: UserSkills;
  location: string;
};
