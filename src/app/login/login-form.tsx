"use client";

import {
  Button,
  Card,
  CardBody,
  Input,
  Link,
  Tab,
  Tabs,
} from "@nextui-org/react";
import { useRouter } from "next/navigation";
import { useState } from "react";
import { useAuth } from "../context/AuthContext";

export default function LoginForm() {
  const router = useRouter();

  const { setToken } = useAuth();

  const [selected, setSelected] = useState("login");
  const [signInError, setSignInError] = useState("");
  const [signUpError, setSignUpError] = useState("");

  async function signIn(formData: FormData) {
    try {
      const res = await fetch(`/api/auth/login`, {
        method: "POST",
        body: JSON.stringify({
          nickname: formData.get("nickname"),
          password: formData.get("password"),
        }),
      });
      const data = await res.json();
      if (res.ok) {
        setToken(data.token);
        router.push("/");
        router.refresh();
      } else {
        return setSignInError(data.message);
      }
    } catch (error) {
      console.error(error);
    }
  }

  async function signUp(formData: FormData) {
    try {
      const res = await fetch(`/api/auth/signup`, {
        method: "POST",
        body: JSON.stringify({
          nickname: formData.get("nickname"),
          password: formData.get("password"),
        }),
      });
      const data = await res.json();
      if (res.ok) {
        setToken(data.token);
        router.push("/");
        router.refresh();
      } else {
        return setSignUpError(data.message);
      }
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <div className="flex flex-col w-full items-center">
      <Card className="max-w-full w-[340px]">
        <CardBody className="overflow-hidden">
          <Tabs
            fullWidth
            size="md"
            aria-label="Tabs form"
            selectedKey={selected}
            onSelectionChange={(key) => {
              setSignInError("");
              setSignUpError("");
              setSelected(key as string);
            }}
          >
            <Tab key="login" title="Login">
              <form className="flex flex-col gap-4" action={signIn}>
                <Input
                  isRequired
                  label="Nickname"
                  name="nickname"
                  autoComplete="username"
                  placeholder="Enter your nickname"
                  type="text"
                />
                <Input
                  isRequired
                  label="Password"
                  autoComplete="current-password"
                  name="password"
                  placeholder="Enter your password"
                  isInvalid={!!signInError}
                  errorMessage={signInError}
                  type="password"
                />
                <p className="text-center text-small">
                  Need to create a new Character ?
                  <Link
                    className="ml-2"
                    size="sm"
                    onPress={() => setSelected("sign-up")}
                  >
                    Sign up
                  </Link>
                </p>
                <div className="flex gap-2 justify-end">
                  <Button fullWidth type="submit" color="primary">
                    Login
                  </Button>
                </div>
              </form>
            </Tab>
            <Tab key="sign-up" title="Sign up">
              <form className="flex flex-col gap-4" action={signUp}>
                <Input
                  isRequired
                  label="Nickname"
                  name="nickname"
                  autoComplete="username"
                  placeholder="Enter your nickname"
                  isInvalid={!!signUpError}
                  errorMessage={signUpError}
                  type="text"
                />
                <Input
                  isRequired
                  label="Password"
                  autoComplete="current-password"
                  name="password"
                  placeholder="Enter your password"
                  type="password"
                />
                <p className="text-center text-small">
                  Already have a Character?
                  <Link
                    className="ml-2"
                    size="sm"
                    onPress={() => setSelected("login")}
                  >
                    Login
                  </Link>
                </p>
                <div className="flex gap-2 justify-end">
                  <Button type="submit" fullWidth color="primary">
                    Sign up
                  </Button>
                </div>
              </form>
            </Tab>
          </Tabs>
        </CardBody>
      </Card>
    </div>
  );
}
