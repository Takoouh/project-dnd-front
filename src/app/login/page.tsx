import LoginForm from "./login-form";

export default function Login() {
  return (
    <>
      <h1 className="text-center text-4xl uppercase pt-10 mb-10">Login</h1>
      <LoginForm />
    </>
  );
}
