"use client";

import { Card, CardBody, CardHeader } from "@nextui-org/react";
import { useAuth } from "../context/AuthContext";
import { apiFetch } from "@/utils/api";
import { useRouter } from "next/navigation";

export const LocationCard = ({ name, id }: { name: string; id: string }) => {
  const { user, setUser, token } = useAuth();
  const router = useRouter();

  const isUserLocation = user!.location === id;

  const movePlayerToLocation = async () => {
    try {
      const userMovedInLocation = await apiFetch(
        `users/me/move/${id}`,
        token!,
        {
          method: "PATCH",
        }
      );
      setUser(userMovedInLocation);
      router.push("/");
    } catch (error) {}
  };

  return (
    <Card
      isPressable={!isUserLocation}
      onPress={() => movePlayerToLocation()}
      className={`${isUserLocation ? "bg-green-600	" : ""}py-4`}
    >
      <CardHeader className="pb-0 pt-2 px-4 flex-col items-middle">
        <h4 className="font-bold text-large">{name}</h4>
      </CardHeader>
      <CardBody className="overflow-visible py-2"></CardBody>
    </Card>
  );
};
