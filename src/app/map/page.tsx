import { fetchWithAuth } from "@/utils/fetch-with-auth";
import { LocationCard } from "./location-card";

export const dynamic = "force-dynamic";

async function getAvailableStrates(): Promise<{ name: string; id: string }[]> {
  try {
    const res = await fetchWithAuth("areas");
    return res;
  } catch (error) {
    console.error("issue fetching available areas", error);
    return [];
  }
}

export default async function Map() {
  const availableStrates = await getAvailableStrates();

  return (
    <>
      <h1>Map :</h1>
      <div className="mt-5 gap-2 grid grid-cols-2 sm:grid-cols-4">
        {availableStrates.map((area) => {
          return <LocationCard id={area.id} name={area.name} key={area.id} />;
        })}
      </div>
    </>
  );
}
