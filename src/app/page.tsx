"use client";

import { Loader } from "@/components/Loader";
import { apiFetch } from "@/utils/api";
import { Card, CardHeader } from "@nextui-org/react";
import { useEffect, useState } from "react";
import { useAuth } from "./context/AuthContext";
import { AreaAction } from "./lib/enums/area-action.enum";
import SkillList from "./ui/skill-list/skill-list";

export default function Home() {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [areaActions, setAreaActions] = useState<AreaAction[]>([]);

  const { token, isAuthLoading, user } = useAuth();

  useEffect(() => {
    const fetchAreaActions = async () => {
      const getAreaAvailableActions = await apiFetch(
        `areas/${user!.location}/available-actions`,
        token!
      );
      setAreaActions(getAreaAvailableActions);
      setIsLoading(false);
    };

    fetchAreaActions();
  }, []);

  if (isLoading || isAuthLoading || !user) {
    return <Loader />;
  }
  return (
    <div className="mt-5 grid grid-cols-5">
      <div className="col-start-2">
        <h2>You are in {user.location}</h2>
        {areaActions.map((action) => (
          <Card
            key={action}
            isPressable
            onPress={() => console.log("item pressed")}
            className="my-2 py-4 w-full"
          >
            <CardHeader className="pb-0 pt-2 px-4 flex-col items-middle">
              <h4 className="font-bold text-large">{action}</h4>
            </CardHeader>
          </Card>
        ))}
      </div>
      <div className="col-start-4">
        <SkillList user={user} />
      </div>
    </div>
  );
}
