"use client";

import {
  Button,
  Navbar,
  NavbarContent,
  NavbarMenu,
  NavbarMenuItem,
  NavbarMenuToggle,
} from "@nextui-org/react";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";
import { useState } from "react";
import { useAuth } from "../../context/AuthContext";

export default function Menu() {
  const router = useRouter();
  const { setToken, token } = useAuth();

  const pathname = usePathname();

  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const logOut = () => {
    localStorage.clear();
    sessionStorage.clear();
    setToken(null);
    router.push("/login");
    router.refresh();
  };

  const links = [
    { name: "Home", href: "/" },
    { name: "Map", href: "/map" },
    { name: "Woodcutting", href: "/woodcutting" },
  ];

  if (pathname === "/login") return;

  return (
    <Navbar onMenuOpenChange={setIsMenuOpen}>
      <NavbarMenuToggle
        className="sm:hidden"
        aria-label={isMenuOpen ? "Close menu" : "Open menu"}
      />
      <NavbarContent justify="center" className="hidden sm:flex justify-right">
        {links.map((item, index) => (
          <NavbarMenuItem
            isActive={pathname === item.href}
            key={`${item.name}-${index}`}
          >
            <Link color="foreground" className="w-full" href={item.href}>
              {item.name}
            </Link>
          </NavbarMenuItem>
        ))}
        <NavbarMenuItem>
          <Button color="danger" className="w-full" onClick={() => logOut()}>
            Logout
          </Button>
        </NavbarMenuItem>
      </NavbarContent>

      <NavbarMenu className="sm:hidden">
        {links.map((item, index) => (
          <NavbarMenuItem
            isActive={pathname === item.href}
            key={`${item.name}-${index}`}
          >
            <Link color="foreground" className="w-full" href="item.href">
              {item.name}
            </Link>
          </NavbarMenuItem>
        ))}
        <NavbarMenuItem>
          <Button color="danger" className="w-full" onClick={() => logOut()}>
            Logout
          </Button>
        </NavbarMenuItem>
      </NavbarMenu>
    </Navbar>
  );
}
