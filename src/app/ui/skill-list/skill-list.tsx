import { User } from "@/app/lib/types/user.type";
import {
  Card,
  CardBody,
  CardHeader,
  Divider,
  Progress,
} from "@nextui-org/react";

export default function SkillList({ user }: { user: User }) {
  return (
    <Card className="md:col-start-3 md:col-span-2">
      <CardHeader className="flex gap-3">
        <div className="flex flex-col">
          <p className="text-md">{user.nickname}</p>
        </div>
      </CardHeader>
      <Divider />
      <CardBody>
        <p className="flex justify-between my-3">
          <span>Cooking: Lv.{user.skills.cooking.level}</span>
          <span>
            {user.skills.cooking.xp}/{user.skills.cooking.nextLevelXp}
          </span>
        </p>
        <Progress
          aria-label="Cooking xp"
          value={
            (user.skills.cooking.xp / user.skills.cooking.nextLevelXp) * 100
          }
          className="max-w"
        />
        <p className="flex justify-between my-3">
          <span>Fishing: Lv.{user.skills.fishing.level}</span>
          <span>
            {user.skills.fishing.xp}/{user.skills.fishing.nextLevelXp}
          </span>
        </p>
        <Progress
          aria-label="Fishing xp"
          value={
            (user.skills.fishing.xp / user.skills.fishing.nextLevelXp) * 100
          }
          className="max-w"
        />
        <p className="flex justify-between my-3">
          <span>Mining: Lv.{user.skills.mining.level}</span>
          <span>
            {user.skills.mining.xp}/{user.skills.mining.nextLevelXp}
          </span>
        </p>
        <Progress
          aria-label="Mining xp"
          value={(user.skills.mining.xp / user.skills.mining.nextLevelXp) * 100}
          className="max-w"
        />
        <p className="flex justify-between my-3">
          <span>Smithing: Lv.{user.skills.smithing.level}</span>
          <span>
            {user.skills.smithing.xp}/{user.skills.smithing.nextLevelXp}
          </span>
        </p>
        <Progress
          aria-label="Smithing xp"
          value={
            (user.skills.smithing.xp / user.skills.smithing.nextLevelXp) * 100
          }
          className="max-w"
        />
        <p className="flex justify-between my-3">
          <span>Woodcutting: Lv.{user.skills.woodcutting.level}</span>
          <span>
            {user.skills.woodcutting.xp}/{user.skills.woodcutting.nextLevelXp}
          </span>
        </p>
        <Progress
          aria-label="Woodcutting xp"
          value={
            (user.skills.woodcutting.xp / user.skills.woodcutting.nextLevelXp) *
            100
          }
          className="max-w"
        />
      </CardBody>
    </Card>
  );
}
