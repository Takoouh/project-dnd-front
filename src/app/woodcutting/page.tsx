export const dynamic = "force-dynamic";

import { fetchWithAuth } from "@/utils/fetch-with-auth";
import { Tooltip } from "@nextui-org/react";
import Image from "next/image";
import { Tree } from "../lib/types/tree.type";

async function getWoodcuttingTrees(): Promise<Tree[]> {
  try {
    const res = await fetchWithAuth("skills/woodcutting/trees");
    return res.trees;
  } catch (error) {
    console.error("issue fetching woddcutting trees", error);
    return [];
  }
}

export default async function Woodcutting() {
  const trees = await getWoodcuttingTrees();

  return (
    <>
      <h1 className="text-right text-3xl mb-5">Woodcutting</h1>

      <ul className="flex flex-wrap justify-center">
        {trees.map((tree) => (
          <li key={tree.id} className="bg-slate-600 p-3 m-3 w-96 flex flex-row">
            <div className="w-20 h-20 relative mr-3">
              <Image
                src={tree.image}
                alt={tree.name}
                fill
                className="object-contain"
              />
            </div>
            <div className="flex flex-col">
              <h2 className="my-2">{tree.name}</h2>
              <p>xp : {tree.reward.experience}</p>
              <p>level : {tree.requiredLevel}</p>
              <p>respawn time : {tree.respawnTime}s</p>
              <p>Reward</p>
              <div className="flex flex-row">
                {tree.reward.items.map((rewardItem) => {
                  return (
                    <div
                      key={tree.name + "_" + rewardItem.name}
                      className="relative w-8 h-8"
                    >
                      <Tooltip showArrow={true} content={rewardItem.name}>
                        <Image
                          src={rewardItem.image}
                          alt={rewardItem.name}
                          width={30}
                          height={30}
                          className="object-contain"
                        />
                      </Tooltip>
                    </div>
                  );
                })}
              </div>
            </div>
          </li>
        ))}
      </ul>
    </>
  );
}
