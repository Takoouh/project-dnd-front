import { Progress } from "@nextui-org/react";

export function Loader() {
  return (
    <Progress
      size="sm"
      isIndeterminate
      aria-label="Loading..."
      className="max-w"
    />
  );
}
