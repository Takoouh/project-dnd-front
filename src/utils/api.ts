import { redirect } from "next/navigation";

export const apiFetch = async (
  url: string,
  token: string,
  options: RequestInit = {}
) => {
  const response = await fetch(`${process.env.DND_API_URL}/${url}`, {
    credentials: "include",
    ...options,
    headers: {
      ...options.headers,
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  });

  if (response.status === 401) {
    localStorage.clear();
    redirect("/login");
  }

  if (!response.ok) {
    throw new Error("Api Failed to responde");
  }

  return response.json();
};
