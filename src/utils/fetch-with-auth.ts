import { cookies } from "next/headers";

export const fetchWithAuth = async (url: string, options: RequestInit = {}) => {
  const storeCookies = cookies();
  const token = storeCookies.get("accessToken")?.value;
  if (!token) {
    throw new Error("Unauthorized");
  }

  const defaultOptions = {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
    ...options,
  };

  const response = await fetch(
    `${process.env.DND_API_URL}/${url}`,
    defaultOptions
  );

  if (response.status === 401) {
    throw new Error("Unauthorized");
  }

  if (!response.ok) {
    const errorData = await response.json();
    throw new Error(errorData.message || "Something went wrong");
  }

  return response.json();
};
